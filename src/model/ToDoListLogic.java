package model;

import java.util.List;

/**ToDoListを追加、削除するロジック
 * @author 綾子
 *
 */
public class ToDoListLogic {
	public void addition(ToDo toDoList, List<ToDo> list) {
		if(list.isEmpty()) {
			list.add(0,toDoList);
		}else {
			list.add(list.size(), toDoList);//末尾に追加
		}
	}

	public void check(ToDo toDoList){
		toDoList.setDone(true);
	}
}
