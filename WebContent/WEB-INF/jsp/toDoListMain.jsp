<%//todoリストのメインページ %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import = "model.ToDo, java.util.List" %>
    <!-- セッションスコープに保存されたlistを取りだす -->
    <% List<ToDo> list = (List<ToDo>)session.getAttribute("list"); %>
    <% ToDo toDo = (ToDo)session.getAttribute("toDoList"); %>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel = "stylesheet" href="http://localhost:8080/kgcExample/css/style.css">
<title>todoリスト</title>
</head>
<body>

<h1><%=toDo.getToday() %>のやる事リスト</h1>
<!-- trueなら斜線を引いて表示、falseなら通常表示 -->
<%for(int i = 0; i < list.size();i++){ %>
	<%if(list.get(i).isDone()==true){ %>
		<p class="line"><%=list.get(i).getText() %></p>
	<%}else{ %>
		<p><%=list.get(i).getText() %></p>
	<%} %>
<%} %>

<form action="/kgcExample/Main">
<input type = "text" placeholder = "空文字は入力しないでください。" name = "text">
<input type = "submit" value = "やる事リストへ追加">
</form>
<a href="/kgcExample/ToDoListDelete">削除</a><br>
<a href="/kgcExample/ToDoListFinish">アプリケーションの終了</a>
</body>
</html>