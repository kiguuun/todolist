package servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.ToDo;
import model.ToDoListLogic;


/**todoリストの処理に関するコントローラ
 * このtodoリストはMainサーブレットスタート
 * Servlet implementation class Main
 */
@WebServlet("/Main")
public class Main extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//トップ画面で入力されたリクエストパラメータを取得
		request.setCharacterEncoding("UTF-8");
		String text = request.getParameter("text");
		//日付を取得してフォーマットにいれる
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		String today = sdf.format(date);



		//セッションスコープを取得
		HttpSession session = request.getSession();
		List<ToDo> list = (List<ToDo>)session.getAttribute("list");
		//listがnullならlistを新規作成、セッションスコープに保存
		if(list ==null) {
			list = new ArrayList<>();
			session.setAttribute("list", list);
		}


		//入力値チェック
		if(text != null && text.length() != 0) {//textの中に何かはいってれば
			//ToDoインスタンスを生成
			ToDo toDo = new ToDo(text,today);
			ToDoListLogic toDoListLogic = new ToDoListLogic();
			toDoListLogic.addition(toDo, list);//セッションスコープで取得したlistの中にやる事が追加されていく

			//セッションスコープにlist情報、toDo情報を保存
			session.setAttribute("list", list);
			session.setAttribute("toDoList", toDo);
			//メインページにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/toDoListMain.jsp");
			dispatcher.forward(request, response);

		}else {//textに何も入ってなければ
			//toDoListTopPageにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/toDoListTopPage.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
