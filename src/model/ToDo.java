package model;

import java.io.Serializable;

/**ToDoListの内容に関する情報を持つJavaBeans
 * @author 綾子
 *
 */
public class ToDo implements Serializable{
	private String text;
	private String today;
	private boolean isDone;

	public ToDo() {}
	public ToDo(String text, String today) {
		this.text = text;
		this.today = today;
		this.isDone = false;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getToday() {
		return today;
	}
	public void setToday(String today) {
		this.today = today;
	}
	public boolean isDone() {
		return isDone;
	}
	public void setDone(boolean isDone) {
		this.isDone = isDone;
	}



}
