package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.ToDo;
import model.ToDoListLogic;

/**削除に関するコントローラ
 * Servlet implementation class ToDoListDelete
 */
@WebServlet("/ToDoListDelete")
public class ToDoListDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**toDoListDelete.jspに飛ばすだけのdoGetメソッド
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/toDoListDelete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//リクエストパラメータを取得

		request.setCharacterEncoding("UTF-8");
		String str = request.getParameter("chyoice");
		Integer choice;
		//取得したリクエストパラメータがnullでなければInteger型choiceへ変換
		if(str != null) {
			 choice = Integer.parseInt(str);
		}else {
			choice = null;
		}
		//斜線を引くか判定
		if(choice != null) {
			HttpSession session = request.getSession();
			List<ToDo> toDoList = (List<ToDo>)session.getAttribute("list");
			ToDoListLogic logic = new ToDoListLogic();
			//何番目のtoDoに変更を加えるか
			ToDo cheakToDo = toDoList.get(choice);
			//選択されたtoDoのフィールド変数をtrueに変える
			logic.check(cheakToDo);
			//セッションスコープに保存
			session.setAttribute("list", toDoList);
		}
		//toDoListMainへフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/toDoListMain.jsp");
		dispatcher.forward(request, response);

	}

}
