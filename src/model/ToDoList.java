package model;

import java.io.Serializable;

/**ToDoListの内容に関する情報を持つJavaBeans
 * @author 綾子
 *
 */
public class ToDoList implements Serializable{
	private String text;
	private String today;

	public ToDoList() {}
	public ToDoList(String text, String today) {
		this.text = text;
		this.today = today;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getToday() {
		return today;
	}
	public void setToday(String today) {
		this.today = today;
	}



}
