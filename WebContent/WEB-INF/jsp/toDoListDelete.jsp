<!-- 削除に関するビュー -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import= "model.ToDo, java.util.List" %>
    <%
    //セッションスコープに保存されたlistを取り出す
    List<ToDo> list = (List<ToDo>)session.getAttribute("list");
    ToDo toDo = (ToDo)session.getAttribute("toDo");
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel = "stylesheet" href="http://localhost:8080/kgcExample/css/style.css">
<title>todoリスト：削除</title>
</head>
<body>

<p>やり終えた事にチェックを入れてください</p>
<form action="/kgcExample/ToDoListDelete" method="post">
	<!-- trueなら斜線を引いて表示、falseなら通常表示 -->
	<%for(int i = 0; i < list.size(); i++){ %>
		<%if(list.get(i).isDone() == true){ %>
		<p class="line">
			<input type="radio" name="chyoice" value="<%=i %>">:<%=list.get(i).getText() %>
		</p>
		<%}else{ %>
			<p>
			<input type="radio" name="chyoice" value="<%=i %>">:<%=list.get(i).getText() %>
		</p>
		<%} %>
	<%} %>
	<input type="submit" value="削除">
</form>
</body>
</html>